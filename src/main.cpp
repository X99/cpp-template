// STDLIB headers

// third party libraries headers
#include <fmt/core.h>

// project headers
#include "config.h"
#include "main.h"

/**
 * Print the banner, and the version
 *
 * @return The version string.
 */
int main() {
    fmt::print(banner);

    return 0;
}
