FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Paris
RUN apt update -qq && \
    apt install git cmake build-essential openssl libssl-dev python3-pip -y
RUN pip3 install conan
RUN git config --global advice.detachedHead false

WORKDIR /app
COPY . .


RUN mkdir build && \
    cd build && \
    conan profile new default --detect && \
    conan profile update settings.compiler.libcxx=libstdc++11 default && \
    conan install .. && \
    cmake .. && \
    cmake --build .

WORKDIR /app/build

CMD ./bin/Foo
