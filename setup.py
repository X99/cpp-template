import shutil
import os
import signal
import sys

def signal_handler(sig, frame):
    print('\n\n💀 You pressed Ctrl+C, aborting.')
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

banner = """
   █████████                             ███████████                                    ████             █████
  ███░░░░░███     ███         ███       ░█░░░███░░░█                                   ░░███            ░░███
 ███     ░░░     ░███        ░███       ░   ░███  ░   ██████  █████████████   ████████  ░███   ██████   ███████    ██████
░███          ███████████ ███████████       ░███     ███░░███░░███░░███░░███ ░░███░░███ ░███  ░░░░░███ ░░░███░    ███░░███
░███         ░░░░░███░░░ ░░░░░███░░░        ░███    ░███████  ░███ ░███ ░███  ░███ ░███ ░███   ███████   ░███    ░███████
░░███     ███    ░███        ░███           ░███    ░███░░░   ░███ ░███ ░███  ░███ ░███ ░███  ███░░███   ░███ ███░███░░░
 ░░█████████     ░░░         ░░░            █████   ░░██████  █████░███ █████ ░███████  █████░░████████  ░░█████ ░░██████
  ░░░░░░░░░                                ░░░░░     ░░░░░░  ░░░░░ ░░░ ░░░░░  ░███░░░  ░░░░░  ░░░░░░░░    ░░░░░   ░░░░░░
                                                                              ░███
                                                                              █████
                                                                             ░░░░░
"""

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    RED = '\033[31m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def yes_no_question(prompt, as_bool=True):
    answer = ""
    ynprompt = " [{0}Y[es]{1}/{2}N[o]{1}]: ".format(bcolors.OKGREEN, bcolors.ENDC, bcolors.RED)
    while answer.upper() not in {"Y", "YES", "N", "NO"}:
        answer = input(prompt + ynprompt)
    if as_bool == True:
        if answer.upper() in {"Y", "YES"}:
            return True
        else:
            return False
    return answer

def number_question(prompt):
    while True:
        try:
            return int(input(prompt))
        except ValueError:
            pass

print(banner + "\n\n")

project_name = input("{}❓ Enter project name:{} ".format(bcolors.OKGREEN, bcolors.ENDC))
version_major = number_question("{}❓ Enter version major number (X.y.z):{} ".format(bcolors.OKGREEN, bcolors.ENDC))
version_minor = number_question("{}❓ Enter version minor number (x.Y.z):{} ".format(bcolors.OKGREEN, bcolors.ENDC))
version_patch = number_question("{}❓ Enter version patch number (x.y.Z):{} ".format(bcolors.OKGREEN, bcolors.ENDC))
enable_benchmarks = yes_no_question("{}❓ Enable Google Benchmarks?{}".format(bcolors.OKGREEN, bcolors.ENDC))
enable_tests = yes_no_question("{}❓ Enable Google Tests?{}".format(bcolors.OKGREEN, bcolors.ENDC))
enable_docs = yes_no_question("{}❓ Enable Doxygen docs?{}".format(bcolors.OKGREEN, bcolors.ENDC))
keep_dockerfile = yes_no_question("{}❓ Keep Dockerfile?{}".format(bcolors.OKGREEN, bcolors.ENDC))
keep_unused_stuff = yes_no_question("{}❓ Keep unused stuff?{}".format(bcolors.OKGREEN, bcolors.ENDC))

print("\nSUMMARY -------------------------------------------")
print("{}👉 Project name: {}{}".format(bcolors.OKGREEN, bcolors.ENDC, project_name))
print("{}👉 Project version: {}{}.{}.{}".format(bcolors.OKGREEN, bcolors.ENDC, version_major, version_minor, version_patch))
print("{}👉 Google benchmark: {}{}".format(bcolors.OKGREEN, bcolors.ENDC, "YES" if enable_benchmarks is True else "NO"))
print("{}👉 Google Tests: {}{}".format(bcolors.OKGREEN, bcolors.ENDC, "YES" if enable_tests is True else "NO"))
print("{}👉 Doxygen docs: {}{}".format(bcolors.OKGREEN, bcolors.ENDC, "YES" if enable_docs is True else "NO"))
print("{}👉 Keep Dockerfile: {}{}".format(bcolors.OKGREEN, bcolors.ENDC, "YES" if keep_dockerfile is True else "NO"))
print("{}👉 Keep unused stuff: {}{}".format(bcolors.OKGREEN, bcolors.ENDC, "YES" if keep_unused_stuff is True else "NO"))
proceed = yes_no_question("\n❓ Proceed?")

if proceed == True:
    # Edit CMakeLists.txt
    lines = []
    with open("CMakeLists.txt.template", "rt") as input_file:
        lines = input_file.readlines()

    # suboptimal loop, to fix in next release
    for index, line in enumerate(lines):
        lines[index] = lines[index].replace("{{FOO}}", project_name.upper())
        lines[index] = lines[index].replace("{{Foo}}", project_name.capitalize())
        lines[index] = lines[index].replace("{{X}}", str(version_major))
        lines[index] = lines[index].replace("{{Y}}", str(version_minor))
        lines[index] = lines[index].replace("{{Z}}", str(version_patch))

        if line == "add_subdirectory(docs)\n":
            if enable_docs == False:
                lines[index] = "# " + line if keep_unused_stuff == True else ""

        if line == "add_subdirectory(benchmark)\n":
            if enable_benchmarks == False:
                lines[index] = "# " + line if keep_unused_stuff == True else ""

        if line == "add_subdirectory(test)\n":
            if enable_tests == False:
                lines[index] = "# " + line if keep_unused_stuff == True else ""

    with open("CMakeLists.txt", "wt") as output_file:
        output_file.writelines(lines)

    if keep_unused_stuff == False:
        if enable_benchmarks == False:
            shutil.rmtree("benchmark")
        if enable_tests == False:
            shutil.rmtree("test")
        if enable_docs == False:
            shutil.rmtree("docs")

    if keep_dockerfile == False:
        os.remove("Dockerfile")
        os.remove(".dockerignore")

    if os.path.exists("build"):
        shutil.rmtree("build")
    os.mkdir("build")

    print("\n⭐️ Setup complete! Have fun!")

else:
    print("\n💀 Setup aborted!")
