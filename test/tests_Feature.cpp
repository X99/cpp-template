#include <gtest/gtest.h>

TEST(tests_Feature, Test1) {
    EXPECT_EQ(1, 1);
}

TEST(tests_Feature, Test2) {
    EXPECT_EQ(1, 1) << "This message will display on error " << 42;
}


int main(int argc, char **argv)  {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
