# ----------------------------------------------------------
# Unit Tests
# ----------------------------------------------------------
add_custom_target(run_tests
    COMMAND ${TESTS} --gtest_output="xml:report.xml"
    WORKING_DIRECTORY ${CMAKE_PROJECT_DIR}
)

include(FetchContent)
include(GoogleTest)

FetchContent_Declare(
    googletest
    GIT_REPOSITORY https://github.com/google/googletest.git
    GIT_TAG        release-1.12.1
)
# For Windows: Prevent overriding the parent project's compiler/linker settings
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)

# Now simply link against gtest or gtest_main as needed. Eg
add_executable(
    ${TESTS}
    tests_Feature.cpp
)
target_link_libraries(
    ${TESTS}
    ${CONAN_LIBS}
    gtest_main
)

gtest_discover_tests(${TESTS})