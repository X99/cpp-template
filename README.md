# How to use this template

## Install dependencies

### macOS

```
brew install google-benchmark googletest plantuml graphviz
```

### Ubuntu

```
sudo apt install libbenchmark-dev libgtest-dev
```

## Setting up

- replace 'Foo' with your project's name, CamelCased
- replace 'FOO' with your project's name, uppercased

## Compiling

```
mkdir build && cd build

# create conan profile
conan profile new default --detect

# optional, sets default stdlib
conan profile update settings.compiler.libcxx=libstdc++11 default

# install dependencies
conan install ..
cmake ..
cmake --build .
```

# Credits

- Icon by by [alkhalifi design @Flaticon](https://www.flaticon.com/premium-icon/template_3681409)
